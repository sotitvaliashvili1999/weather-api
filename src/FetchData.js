export const getCityWeather = async city => {
  const request = await fetch(`https://goweather.herokuapp.com/weather/${city}`);
  return await request.json();
};
