import { useEffect, useState } from "react";
import "./App.css";
import { getCityWeather } from "./FetchData";
function App() {
  const [cityWeather, setCityWeather] = useState({});
  const [searchCity, setSearchCity] = useState("");
  const [query, setQuery] = useState("");
  useEffect(() => {
    getCityWeather(searchCity).then(weatherData => setCityWeather(weatherData));
  }, [searchCity]);

  const queryChanged = event => {
    setQuery(event.target.value);
  };
  const searchBtnClicked = () => {
    setSearchCity(query);
    setQuery("");
  };
  return (
    <div className="app">
      <div className="inputs__wrapper">
        <input
          type="text"
          className="search__input"
          placeholder="search city..."
          value={query}
          onChange={queryChanged}
        />
        <input
          type="button"
          className="search__button"
          value="Search"
          onClick={searchBtnClicked}
        />
      </div>
      {cityWeather.temperature && (
        <table class="weather__table">
          <thead>
            <tr>
              <th>City</th>
              <th>Temperature</th>
              <th>Wind</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{searchCity}</td>
              <td>{cityWeather.temperature}</td>
              <td>{cityWeather.wind}</td>
              <td>{cityWeather.description}</td>
            </tr>
          </tbody>
        </table>
      )}
    </div>
  );
}

export default App;
